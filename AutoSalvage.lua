AutoSalvage = {}

local selstatname = nil
local statCount = 19
local timerOnUpdate = 10
local STATS = {
	STRENGTH = 1,
	--AGILITY = 2,
	WILLPOWER = 3,
	TOUGHNESS = 4,
	WOUNDS = 5,
	INITIATIVE = 6,
	WEAPON_SKILL = 7,
	BALLISTIC_SKILL = 8,
	INTELLIGENCE = 9,
	--BLOCK = 10,
	--PARRY = 11,
	--DODGE = 12,
	--DISRUPT = 13,
	SPIRIT_RESIST = 14,
	ELEMENTAL_RESIST = 15,
	CORPOREAL_RESIST = 16,
	MELEE_CRIT_CHANCE = 76,
	RANGED_CRIT_CHANCE = 77,
	MAGIC_CRIT_CHANCE = 78,
	MELEE_POWER = 80,
	RANGED_POWER = 81,
	MAGIC_POWER = 82,
	HEALING_CRIT_CHANCE = 89,	
	HEALING_POWER = 94
}

local function print(txt)
	EA_ChatWindow.Print(towstring(txt))
end

local function IsItemSalvagable(itemData)
	local isSalvagable = false
	if(	itemData ~= nil and
		itemData.name ~= L"" and
		itemData.broken == false and
		SalvagingWindow.IsSalvagableItem(itemData) == true and
		SalvagingWindow.PlayerHasSufficientSkillToSalvageItem(itemData) == true and
		EA_Window_Backpack.IsSlotLocked(selectedSlot, selectedBackpack) == false
	) then
		isSalvagable = true
	end
	
	return isSalvagable
end

local function GetStat(itemData)
	local s = 1
	while(s <= statCount) do -- number of items in AutoSalvage.savedSettings.salvageOrder
		--[===[@debug@
		d("AutoSalvage debug: Checking stat: " .. tostring(s))
		--@end-debug@]===]
		local b = 1
		while(itemData.bonus[b] ~= nil and itemData.bonus[b].reference ~= nil) do
			--[===[@debug@
			d("AutoSalvage debug: Checking bonus: " .. tostring(b))
			--@end-debug@]===]
			local selectedStat = itemData.bonus[b].reference
			if(selectedStat == AutoSalvage.savedSettings.salvageOrder[s]) then
				--[===[@debug@
				d("AutoSalvage debug: We matched a stat.")
				--@end-debug@]===]
				-- matched a stat
				selstatname = CraftingUtils.GetSalvagingStatString(itemData.bonus[b].reference)
				return selectedStat
			end
			b = b + 1
		end
		s = s + 1
	end
	
end

function AutoSalvage.OnInitialize()
	-- register slash command.
	if (LibSlash ~= nil) then
		LibSlash.RegisterSlashCmd("asl", AutoSalvage.OnSlashCommand)
	end
	if(AutoSalvage.savedSettings == nil or AutoSalvage.savedSettings.version == nil or AutoSalvage.savedSettings.version == 1) then
		AutoSalvage.savedSettings = {}
		AutoSalvage.savedSettings.version = 2
		AutoSalvage.savedSettings.salvageOrder = {}
		AutoSalvage.savedSettings.salvageOrder[1] = STATS.HEALING_CRIT_CHANCE
		AutoSalvage.savedSettings.salvageOrder[2] = STATS.HEALING_POWER
		AutoSalvage.savedSettings.salvageOrder[3] = STATS.MAGIC_CRIT_CHANCE
		AutoSalvage.savedSettings.salvageOrder[4] = STATS.MELEE_CRIT_CHANCE
		AutoSalvage.savedSettings.salvageOrder[5] = STATS.RANGED_CRIT_CHANCE
		AutoSalvage.savedSettings.salvageOrder[6] = STATS.MAGIC_POWER
		AutoSalvage.savedSettings.salvageOrder[7] = STATS.MELEE_POWER
		AutoSalvage.savedSettings.salvageOrder[8] = STATS.RANGED_POWER
		AutoSalvage.savedSettings.salvageOrder[9] = STATS.TOUGHNESS
		AutoSalvage.savedSettings.salvageOrder[10] = STATS.STRENGTH
		AutoSalvage.savedSettings.salvageOrder[11] = STATS.WILLPOWER
		AutoSalvage.savedSettings.salvageOrder[12] = STATS.WOUNDS
		AutoSalvage.savedSettings.salvageOrder[13] = STATS.BALLISTIC_SKILL
		AutoSalvage.savedSettings.salvageOrder[14] = STATS.INTELLIGENCE
		AutoSalvage.savedSettings.salvageOrder[15] = STATS.WEAPON_SKILL
		AutoSalvage.savedSettings.salvageOrder[16] = STATS.INITIATIVE
		AutoSalvage.savedSettings.salvageOrder[17] = STATS.CORPOREAL_RESIST
		AutoSalvage.savedSettings.salvageOrder[18] = STATS.SPIRIT_RESIST
		AutoSalvage.savedSettings.salvageOrder[19] = STATS.ELEMENTAL_RESIST
	end
	
	AutoSalvage.isTurboEnabled = false
	AutoSalvage.isEnabled = false
	--timerOnUpdate = 60  -- not really needed as it checks for enabled first.
end

function AutoSalvage.OnUpdate(elapsed)
	if(not AutoSalvage.isEnabled) then
		return
	end
	timerOnUpdate = timerOnUpdate - elapsed
	if(timerOnUpdate >= 1) then
		return
	end
	
	-- we out of wait time, so find something to salvage.
	local selectedBackpack = EA_Window_Backpack.TYPE_INVENTORY  -- we have backpack
	local inventory = EA_Window_Backpack.GetItemsFromBackpack(selectedBackpack)
	local numBags = 4
	if(numBags > 4) then  -- needed due to no extra bag at rank 40
		numbags = 4
	end
	local numSlots = numBags * 16
	--[===[@debug@
	d("AutoSalvage debug: Number of bags found: " .. tostring(numBags))
	d("AutoSalvage debug: Number of slots found: " .. tostring(numSlots))
	--@end-debug@]===]
	local selectedSlot = 1
	while(selectedSlot <= numSlots) do -- looping through slots
		--[===[@debug@
		d("AutoSalvage debug: Current slot is: " .. tostring(selectedSlot))
		--@end-debug@]===]
		
		local itemData = inventory[selectedSlot]
		local selectedStat = nil
		
		--[===[@debug@
		if(itemData ~= nil and itemData.name ~= nil and itemData.name ~= L"") then
			d("AutoSalvage debug: Current item is: " .. tostring(itemData.name))
		end
		--@end-debug@]===]
		
		if(IsItemSalvagable(itemData)) then
			-- see if it's a figurine
			--if(itemData.uniqueID >= 908888 and itemData.uniqueID <= 908897) then
				-- if it's uncommon set stat to 0, otherwise find according to list.
				if(itemData.rarity == SystemData.ItemRarity.UNCOMMON) then
					selectedStat = 0
				else
					selectedStat = GetStat(itemData)
				end
			elseif(itemData.rarity == SystemData.ItemRarity.UNCOMMON) then
				selectedStat = GetStat(itemData)
			--end
		end
		
		if(selectedStat ~= nil) then
			local salvagingType  -- we have backpack, slot, stat and type
			if(itemData.flags[GameData.Item.EITEMFLAG_MAGICAL_SALVAGABLE]) then
				salvagingType = GameData.SalvagingTypes.MAGICAL
			else
				salvagingType = GameData.SalvagingTypes.MUNDANE
			end
			
			if(RequestSalvageItem(selectedSlot, selectedBackpack, salvagingType, selectedStat)) then
				--LayerTimerWindow.SetActionName(towstring(tostring(GetString(StringTables.Default.LABEL_SKILL_SALVAGING))..": "..tostring(itemData.name)), true )
				LayerTimerWindow.SetActionName(towstring(tostring(itemData.name)..": "..tostring(selstatname)), true )
			end
			selectedSlot = 1
			timerOnUpdate = 5
			return
		else
			selectedSlot = selectedSlot + 1
		end
		
	end
	
	timerOnUpdate = 30  -- nothing found wait longer before next attempt.
	if(AutoSalvage.isTurboEnabled == true) then
		timerOnUpdate = 5
	end
end

function AutoSalvage.OnShutdown()
	
end

function AutoSalvage.ToggleEnabled()
	AutoSalvage.isEnabled = not AutoSalvage.isEnabled
	if(AutoSalvage.isEnabled) then
		timerOnUpdate = 10
		if(AutoSalvage.isTurboEnabled == true) then
			timerOnUpdate = 1
		end
		print("AutoSalvage is now enabled, and will begin in"..timerOnUpdate.."seconds.")
	else
		print("AutoSalvage is now disabled.")
		--timerOnUpdate = 60  -- not really needed as it checks for enabled first.
	end
end

function AutoSalvage.ToggleTurbo()
	AutoSalvage.isTurboEnabled = not AutoSalvage.isTurboEnabled
	if(AutoSalvage.isTurboEnabled) then
		print("AutoSalvage Turbo Mode is now enabled.")
	else
		print("AutoSalvage Turbo Mode is now disabled.")
	end
end

function AutoSalvage.OnSlashCommand(opt)
	-- toggle options window
	if(WindowGetShowing("AutoSalvageOptions")) then
		AutoSalvage.OptionsWindow.WindowClose()
	else
		AutoSalvage.OptionsWindow.WindowOpen()
	end
end

