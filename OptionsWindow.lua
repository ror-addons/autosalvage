if(AutoSalvage == nil) then
	return	-- we shouldn't be here because AutoSalvage should exist if WAR found this file.
end
if(AutoSalvage.OptionsWindow == nil) then
	AutoSalvage.OptionsWindow = {}
end
local statOrder = {}
local parentBody = "AutoSalvageOptionsBody"
local statCount = 19

function AutoSalvage.OptionsWindow.Update()
	-- load statOrder into window.
	
	local s = 1
	while(s <= statCount) do
		local text = tostring(CraftingUtils.GetSalvagingStatString(statOrder[s]))
		text = string.sub(text, 2, (string.len(text) - 6))
		LabelSetText(parentBody.."Stat"..s.."Label", towstring(text))
		s = s + 1
	end
end

function AutoSalvage.OptionsWindow.CheckBoxToggle()
	-- check the checkbox, or uncheck it
	local checkBoxName = SystemData.ActiveWindow.name
	local Status = not ButtonGetCheckButtonFlag(checkBoxName)
	ButtonSetCheckButtonFlag(checkBoxName, Status)
	ButtonSetPressedFlag(checkBoxName, Status)
	
	-- change statOrder to new settings
	local s = 1
	local firstStatPosition = 0
	local secondStatPosition = 0
	while(s <= statCount) do
		if(ButtonGetCheckButtonFlag(parentBody.."Stat"..s.."CheckBox")) then
			if(firstStatPosition ~= 0 and secondStatPosition == 0) then
				secondStatPosition = s
			end
			if(firstStatPosition == 0) then
				firstStatPosition = s
			end
		end
		s = s + 1
	end
		
	if(firstStatPosition ~= 0 and secondStatPosition ~= 0) then
		-- we have the stats to swap, swap them and save to settings.
		local firstStat = statOrder[firstStatPosition]
		local secondStat = statOrder[secondStatPosition]
		statOrder[firstStatPosition] = secondStat
		statOrder[secondStatPosition] = firstStat
		
		AutoSalvage.OptionsWindow.SaveSettings()
		
		-- uncheck the CheckBoxes
		s = 1
		while(s <= statCount) do
			ButtonSetCheckButtonFlag(parentBody.."Stat"..s.."CheckBox", false)
			ButtonSetPressedFlag(parentBody.."Stat"..s.."CheckBox", false)
			s = s + 1
		end
		
		-- Update the window to the new statOrder
		AutoSalvage.OptionsWindow.Update()
	end
end

function AutoSalvage.OptionsWindow.LoadSettings()
	local s = 1
	while(s <= statCount) do
		statOrder[s] = AutoSalvage.savedSettings.salvageOrder[s]
		s = s + 1
	end
	AutoSalvage.OptionsWindow.Update()
end

function AutoSalvage.OptionsWindow.SaveSettings()
	local s = 1
	while(s <= statCount) do
		AutoSalvage.savedSettings.salvageOrder[s] = statOrder[s]
		s = s + 1
	end
end

function AutoSalvage.OptionsWindow.OnInitialize()
    LabelSetText("AutoSalvageOptionsTitleText", L"Auto Salvage")
    ButtonSetText(parentBody.."TurboButton", L"Toggle Turbo Mode")
    ButtonSetText(parentBody.."ToggleButton", L"Toggle Processing")
	local s = 1
	while(s <= statCount) do
		LabelSetText(parentBody.."Stat"..s.."Position", towstring(s))
		ButtonSetCheckButtonFlag(parentBody.."Stat"..s.."CheckBox", false)
		ButtonSetPressedFlag(parentBody.."Stat"..s.."CheckBox", false)
		s = s + 1
	end
	AutoSalvage.OptionsWindow.LoadSettings()
end

function AutoSalvage.OptionsWindow.WindowOpen()
	AutoSalvage.OptionsWindow.LoadSettings()
	WindowSetShowing("AutoSalvageOptions", true)
end

function AutoSalvage.OptionsWindow.WindowClose()
	WindowSetShowing("AutoSalvageOptions", false)
	AutoSalvage.OptionsWindow.SaveSettings()
end
