<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="AutoSalvage" version="1.1.0" date="1/8/2009" > 
    <!-- remember day/month/year -->	
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="laconic" email="" />		
    <Description text="Addon to automatically salvage items in backpack." />		 		
    <Dependencies>			
      <Dependency name="EA_BackpackWindow"		optional="false"	forceEnable="true"	/>			
      <Dependency name="EA_CastTimerWindow"		optional="false"	forceEnable="true"	/>			
      <!-- LayerTimerWindow.SetActionName() -->			
      <Dependency name="EA_CraftingSystem"		optional="false"	forceEnable="true"	/>			
      <Dependency name="LibSlash"		optional="false"	forceEnable="true"	/>		
    </Dependencies>		
    <Files>			
      <File name="AutoSalvage.lua" />			
      <File name="OptionsWindow.lua" />			
      <File name="OptionsWindow.xml" />		
    </Files>		 		
    <OnInitialize>			
      <CallFunction name="AutoSalvage.OnInitialize" />			
      <CreateWindow name="AutoSalvageOptions" show="false" />		
    </OnInitialize>		
    <OnUpdate>			
      <CallFunction name="AutoSalvage.OnUpdate" />		
    </OnUpdate>		
    <OnShutdown>			
      <CallFunction name="AutoSalvage.OnShutdown" />		
    </OnShutdown>			 		
    <SavedVariables>			
      <SavedVariable name="AutoSalvage.savedSettings" global="true" />		
    </SavedVariables>		 	
  </UiMod>
</ModuleFile>